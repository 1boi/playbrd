﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace playbrd.UI {
    class Program {
        [STAThread]
        public static void Main() {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault( false );

            System.Windows.Forms.Application.Run( new Runner() );
        }
    }
}
