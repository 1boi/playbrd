﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Advanced_Combat_Tracker;

namespace playbrd {
    public class ActPlugin : Advanced_Combat_Tracker.IActPluginV1 {
        public void DeInitPlugin() {
            if( this.actPlugin is null ) return;
            this.mainThread?.Abort();
            this.mainThread = null;
        }

        System.Windows.Forms.Label pluginStatusText;
        System.Threading.Thread mainThread;
        FFXIV_ACT_Plugin.FFXIV_ACT_Plugin actPlugin;
        ida.FFXIV.Client client;
        Automation.Player player;

        public void InitPlugin( System.Windows.Forms.TabPage pluginScreenSpace, System.Windows.Forms.Label pluginStatusText ) {
            this.pluginStatusText = pluginStatusText;
            if( !( Advanced_Combat_Tracker.ActGlobals.oFormActMain is null ) ) {
                this.actPlugin = ( FFXIV_ACT_Plugin.FFXIV_ACT_Plugin )Advanced_Combat_Tracker.ActGlobals.oFormActMain.ActPlugins.FirstOrDefault( x => x.pluginObj.GetType() == typeof( FFXIV_ACT_Plugin.FFXIV_ACT_Plugin ) )?.pluginObj;
                if( this.actPlugin is null ) {
                    this.pluginStatusText.Text = "root plugin unavailable";
                }
            }

            var b = new System.Windows.Forms.Button();
            b.Text = "start";
            b.Location = new System.Drawing.Point( 20, 20 );
            b.Size = new System.Drawing.Size( 100, 24 );
            b.Click += this.B_Click;
            pluginScreenSpace.Controls.Add( b );

            b = new System.Windows.Forms.Button();
            b.Text = "abort";
            b.Location = new System.Drawing.Point( 20, 50 );
            b.Size = new System.Drawing.Size( 100, 24 );
            b.Click += this.C_Click;
            pluginScreenSpace.Controls.Add( b );
        }

        private void C_Click( object sender, EventArgs e ) {
            if( this.mainThread is null ) return;
            this.mainThread.Abort();
            this.mainThread = null;
        }

        private void B_Click( object sender, EventArgs e ) {
            this.mainThread = new System.Threading.Thread( Run );
            this.mainThread.Name = nameof( playbrd ) + nameof( Run );
            this.mainThread.IsBackground = true;
            this.mainThread.Start();
        }

        private void Run() {
            //FIXME: dev version should use local paths
            var miPath = string.Empty; //= @"C:\whatever\mi.xml"

            //download mi.xml from repo
            if( string.IsNullOrEmpty( miPath ) ) {
                var miUrl = "https://bitbucket.org/1boi/playbrd/raw/0392b0917c2e9ddef8d0e3fac016943e32deeb23/mi.xml";

                var pluginInfo = Advanced_Combat_Tracker.ActGlobals.oFormActMain.ActPlugins.First( x => x.pluginObj == this );
                miPath = System.IO.Path.Combine( pluginInfo.pluginFile.DirectoryName, "mi.xml" );

                using( var wc = new System.Net.WebClient() ) {
                    wc.DownloadFile( miUrl, miPath );
                }
            }

            var mi = ida.FFXIV.MemoryInformation.Deserialize( miPath );
            this.client = new ida.FFXIV.Client( mi );
            this.player = new Automation.Player( this.client );

            this.pluginStatusText.Invoke( new Action( () => {
                this.pluginStatusText.Text = "running";
            } ) );

            try {
                if( !this.client.Open() ) {
                    do {
                        System.Threading.Thread.Sleep( 1000 );
                    } while( !this.client.Open() );
                    System.Threading.Thread.Sleep( 1000 );
                }

                ida.Native.Win32.SetForegroundWindow( this.client.Process.MainWindowHandle );
                System.Threading.Thread.Sleep( 250 );

                //play brd
                this.player.Initialize( "BRD", "Dummy" );

                while( true ) {
                    if( !this.client.Update() ) break;
                    if( !this.player.Update() ) break;
                    System.Threading.Thread.Sleep( 20 );
                }

            } catch( System.Threading.ThreadAbortException ) {
            } catch( Exception ex ) {
                System.Windows.Forms.MessageBox.Show( ex.ToString() );
            } finally {
                this.client.Close();

                this.pluginStatusText.Invoke( new Action( () => {
                    this.pluginStatusText.Text = "terminated";
                } ) );
            }
        }

    }
}
