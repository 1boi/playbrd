﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace playbrd.Automation.DRG {
    class Dummy {


        private readonly int[] cooldowns;
        private readonly int[] buffs;

        private readonly ida.FFXIV.Client client;

        public Dummy( ida.FFXIV.Client client ) {
            this.client = client;

            this.GCD = 2450;

            this.buffs = new int[Enum.GetValues( typeof( Skill ) ).Length];
            this.cooldowns = new int[this.buffs.Length];

            this.cooldowns[( int )Skill.HeavyThrust] = 30000;

            this.sw = new System.Diagnostics.Stopwatch();
        }

        System.Diagnostics.Stopwatch sw;

        public int GCD {
            get => this.gcd;
            set {
                this.gcd = value;
                this.ogcd = value / 3;
            }
        }
        private int gcd;
        private int ogcd;

        public void Initialize() {
            this.sw.Start();
            int roti = 0;

            if( roti == 0 ) {
                this.rotation = new Skill[] {
                    Skill.HeavyThrust,
                    Skill.ImpulseDrive,
                    Skill.Disembowel,
                    Skill.ChaosThrust,
                    Skill.WheelingThrust,
                    Skill.TrueThrust,
                    Skill.VorpalThrust,
                    Skill.FullThrust,
                    Skill.FangAndClaw,
                    Skill.TrueThrust,
                    Skill.VorpalThrust,
                    Skill.FullThrust,
                    Skill.FangAndClaw,
                };
            } else if( roti == 1 ) {
                this.rotation = new Skill[] {
                    Skill.HeavyThrust,
                    Skill.ImpulseDrive,
                    Skill.Disembowel,
                    Skill.ChaosThrust,
                    Skill.TrueThrust,
                    Skill.VorpalThrust,
                    Skill.FullThrust,
                    Skill.TrueThrust,
                    Skill.VorpalThrust,
                    Skill.FullThrust,
                    Skill.TrueThrust,
                    Skill.VorpalThrust,
                    Skill.FullThrust,
                };
            } else {
                this.rotation = new Skill[] {
                    Skill.HeavyThrust,
                    Skill.TrueThrust,
                    Skill.VorpalThrust,
                    Skill.FullThrust,
                    Skill.TrueThrust,
                    Skill.VorpalThrust,
                    Skill.FullThrust,
                    Skill.TrueThrust,
                    Skill.VorpalThrust,
                    Skill.FullThrust,
                    Skill.TrueThrust,
                    Skill.VorpalThrust,
                    Skill.FullThrust,
                };
            }

        }

        int actionTick = 0;
        int offtick = 0;
        Skill[] rotation;

        public void Update() {
            if( this.client.Entities.Target == null ) {
                this.offtick++;
                if( this.offtick >= 30 * 2.5 ) {
                    this.actionTick = 0;
                    return;
                }
            } else {
                this.offtick = 0;
            }

            switch( this.Activate( this.rotation[this.actionTick] ) ) {
                case 0:
                    System.Threading.Thread.Sleep( this.gcd - this.ogcd );
                    goto case -1;
                case -1:
                    this.actionTick = ( this.actionTick + 1 ) % this.rotation.Length;
                    break;
                case 1:
                    break;
            }
        }

        private bool AllSkillsDisabled() {
            foreach( var ska in ActionMap.Map.Keys ) {
                if( this.GetItemFromHotbar( ska ).IsAvailable ) {
                    return false;
                }
            }

            return true;
        }

        private int Activate( Skill sk ) {
            if( sk == Skill.None ) return -1;

            var item = this.GetItemFromHotbar( sk );

            if( !item.IsAvailable ) {
                if( this.AllSkillsDisabled() ) return 1;
                return -1;
            }

            if( this.client.Entities.Target == null ) return 1;

            if( item.CooldownProgress != 0 ) {
                if( item.CooldownProgress < 5 ) {
                    if( this.cooldowns[( int )sk] != 0 ) this.buffs[( int )sk] = this.GetCooldown( sk );
                    return 0;
                }

                if( item.CostOrRemainingCooldown > this.gcd ) {
                    return 1;
                }
            }

            ActionMap.Map[sk].Execute( this.client );

            return 1;
        }

        private int GetCooldown( Skill sk ) {
            return this.cooldowns[( int )sk];
        }

        private ida.FFXIV.Hud.HotbarItem GetItemFromHotbar( Skill sk ) {
            return this.client.Hud.Hotbars[ActionMap.Map[sk].Hotbar].Slots[ActionMap.Map[sk].Index];
        }
    }
}
