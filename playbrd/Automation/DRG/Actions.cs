﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace playbrd.Automation.DRG {
    class ActionMap {
        public static Dictionary<Skill, SkillActivator> Map { get; } = new Dictionary<Skill, SkillActivator>() {
            { Skill.TrueThrust, new SkillActivator(System.Windows.Forms.Keys.D1, ida.FFXIV.Keyboard.KeyCode.None, 0, 0 ) },
            { Skill.VorpalThrust, new SkillActivator(System.Windows.Forms.Keys.D2, ida.FFXIV.Keyboard.KeyCode.None, 0, 1 ) },
            { Skill.FullThrust, new SkillActivator(System.Windows.Forms.Keys.D3, ida.FFXIV.Keyboard.KeyCode.None, 0, 2 ) },

            { Skill.ImpulseDrive, new SkillActivator(System.Windows.Forms.Keys.D1, ida.FFXIV.Keyboard.KeyCode.Alt, 1, 0 ) },
            { Skill.Disembowel, new SkillActivator(System.Windows.Forms.Keys.D2, ida.FFXIV.Keyboard.KeyCode.Alt, 1, 1 ) },
            { Skill.ChaosThrust, new SkillActivator(System.Windows.Forms.Keys.D3, ida.FFXIV.Keyboard.KeyCode.Alt, 1, 2 ) },
            { Skill.BloodForBlood, new SkillActivator(System.Windows.Forms.Keys.X, ida.FFXIV.Keyboard.KeyCode.None, 1, 4 ) },
            { Skill.FangAndClaw, new SkillActivator(System.Windows.Forms.Keys.Q, ida.FFXIV.Keyboard.KeyCode.None, 1, 7 ) },
            { Skill.WheelingThrust, new SkillActivator(System.Windows.Forms.Keys.F, ida.FFXIV.Keyboard.KeyCode.None, 1, 8 ) },

            { Skill.HeavyThrust, new SkillActivator(System.Windows.Forms.Keys.F1, ida.FFXIV.Keyboard.KeyCode.None, 2, 0 ) },
            { Skill.Jump, new SkillActivator(System.Windows.Forms.Keys.Q, ida.FFXIV.Keyboard.KeyCode.Alt, 2, 7 ) },
            { Skill.LifeSurge, new SkillActivator(System.Windows.Forms.Keys.F, ida.FFXIV.Keyboard.KeyCode.Alt, 2, 8 ) },
            { Skill.PiercingTalon, new SkillActivator(System.Windows.Forms.Keys.R, ida.FFXIV.Keyboard.KeyCode.Alt, 2, 10 ) },
        };
    }

    enum Skill {
        None,
        TrueThrust,
        VorpalThrust,
        ImpulseDrive,
        HeavyThrust,
        PiercingTalon,
        LifeSurge,
        FullThrust,
        BloodForBlood,
        Jump,
        ElusiveJump,
        Disembowel,
        DoomSpike,
        SpineshatterDive,
        ChaosThrust,
        DragonfireDive,
        BattleLitany,
        BloodOfTheDragon,
        FangAndClaw,
        WheelingThrust,
        Geirskogul,
        SonicThrust,
        DragonSight,
        MirageDive,
        Nastrond
    }
    enum Buff {
        None = 0,
        HeavyThrust = 0x73,
    }

    class SkillActivator {
        private readonly Random rand;

        public SkillActivator( System.Windows.Forms.Keys key, ida.FFXIV.Keyboard.KeyCode modifier, int hotbar, int index ) {
            this.Key = key;
            this.Modifier = modifier;

            this.Hotbar = hotbar;
            this.Index = index;

            this.rand = new Random();
        }

        public int Hotbar { get; }
        public int Index { get; }

        public System.Windows.Forms.Keys Key { get; }
        public ida.FFXIV.Keyboard.KeyCode Modifier { get; }

        public void Execute( ida.FFXIV.Client client ) {
            if( this.Key == System.Windows.Forms.Keys.None ) return;

            client.Keyboard.SetKey( ida.FFXIV.Keyboard.KeyCode.Alt, this.Modifier == ida.FFXIV.Keyboard.KeyCode.Alt );
            client.Keyboard.SetKey( ida.FFXIV.Keyboard.KeyCode.Shift, this.Modifier == ida.FFXIV.Keyboard.KeyCode.Shift );

            client.Keyboard.SendToggleKey( this.Key, 50 + this.rand.Next( -20, 21 ) );

            client.Keyboard.SetKey( ida.FFXIV.Keyboard.KeyCode.Alt, false );
            client.Keyboard.SetKey( ida.FFXIV.Keyboard.KeyCode.Shift, false );
        }

    }
}
