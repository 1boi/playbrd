﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace playbrd.Automation {
    class Player {
        private readonly ida.FFXIV.Client client;

        public Player( ida.FFXIV.Client client ) {
            this.client = client;
        }

        BRD.Dummy fight;

        public void Initialize( string job, string fight ) {
            this.fight = new BRD.Dummy( client );
            this.fight.Initialize();
        }

        public bool Update() {
            this.fight.Update();
            return true;
        }



    }
}
