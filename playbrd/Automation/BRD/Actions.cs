﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace playbrd.Automation.BRD {

    class ActionMap {
        public static Dictionary<Skill, SkillActivator> Map { get; } = new Dictionary<Skill, SkillActivator>() {
            { Skill.None, new SkillActivator( System.Windows.Forms.Keys.None, ida.FFXIV.Keyboard.KeyCode.None, -1, -1 ) },
            { Skill.HeavyShot, new SkillActivator( System.Windows.Forms.Keys.D1, ida.FFXIV.Keyboard.KeyCode.None, 0, 0 ) },
            { Skill.StraightShot, new SkillActivator( System.Windows.Forms.Keys.D1, ida.FFXIV.Keyboard.KeyCode.Alt, 1, 0 ) },
            { Skill.RagingStrikes, new SkillActivator( System.Windows.Forms.Keys.E, ida.FFXIV.Keyboard.KeyCode.None, 1, 9 ) },
            { Skill.MiserysEnd, new SkillActivator( System.Windows.Forms.Keys.X, ida.FFXIV.Keyboard.KeyCode.None, 1, 4 ) },
            { Skill.Bloodletter, new SkillActivator( System.Windows.Forms.Keys.D4, ida.FFXIV.Keyboard.KeyCode.None, 0, 3 ) },
            { Skill.Barrage, new SkillActivator( System.Windows.Forms.Keys.Q, ida.FFXIV.Keyboard.KeyCode.None, 1, 7 ) },
            { Skill.MagesBallad, new SkillActivator( System.Windows.Forms.Keys.E, ida.FFXIV.Keyboard.KeyCode.Alt, 2, 9 ) },
            { Skill.FoeRequiem, new SkillActivator( System.Windows.Forms.Keys.F2, ida.FFXIV.Keyboard.KeyCode.Alt, 2, 5 ) },
            { Skill.ArmysPaeon, new SkillActivator( System.Windows.Forms.Keys.F, ida.FFXIV.Keyboard.KeyCode.Alt, 2, 8 ) },
            { Skill.RainOfDeath, new SkillActivator( System.Windows.Forms.Keys.F3, ida.FFXIV.Keyboard.KeyCode.None, 2, 2 ) },
            { Skill.BattleVoice, new SkillActivator( System.Windows.Forms.Keys.C, ida.FFXIV.Keyboard.KeyCode.Alt, 2, 11 ) },
            { Skill.TheWanderersMinuet, new SkillActivator( System.Windows.Forms.Keys.R, ida.FFXIV.Keyboard.KeyCode.Alt, 2, 10 ) },
            { Skill.PitchPerfect, new SkillActivator( System.Windows.Forms.Keys.R, ida.FFXIV.Keyboard.KeyCode.None, 1, 10 ) },
            { Skill.EmpyrealArrow, new SkillActivator( System.Windows.Forms.Keys.Q, ida.FFXIV.Keyboard.KeyCode.Alt, 2, 7 ) },
            { Skill.IronJaws, new SkillActivator( System.Windows.Forms.Keys.D2, ida.FFXIV.Keyboard.KeyCode.Alt, 1, 1 ) },
            { Skill.Sidewinder, new SkillActivator( System.Windows.Forms.Keys.V, ida.FFXIV.Keyboard.KeyCode.None, 0, 5 ) },
            { Skill.Troubadour, new SkillActivator( System.Windows.Forms.Keys.F2, ida.FFXIV.Keyboard.KeyCode.None, 2, 1 ) },
            { Skill.CausticBite, new SkillActivator( System.Windows.Forms.Keys.D2, ida.FFXIV.Keyboard.KeyCode.None, 0, 1 ) },
            { Skill.Stormbite, new SkillActivator( System.Windows.Forms.Keys.D3, ida.FFXIV.Keyboard.KeyCode.None, 0, 2 ) },
            { Skill.NaturesMinne, new SkillActivator( System.Windows.Forms.Keys.F1, ida.FFXIV.Keyboard.KeyCode.None, 2, 0 ) },
            { Skill.RefulgentArrow, new SkillActivator( System.Windows.Forms.Keys.D4, ida.FFXIV.Keyboard.KeyCode.Alt, 1, 3 ) },
        };
    }

    enum Skill {
        None,
        HeavyShot,
        StraightShot,
        RagingStrikes,
        VenomousBite,
        MiserysEnd,
        Bloodletter,
        RepellingShot,
        QuickNock,
        WindBite,
        Barrage,
        MagesBallad,
        FoeRequiem,
        ArmysPaeon,
        RainOfDeath,
        BattleVoice,
        TheWanderersMinuet,
        PitchPerfect,
        EmpyrealArrow,
        IronJaws,
        TheWardensPaean,
        Sidewinder,
        Troubadour,
        CausticBite,
        Stormbite,
        NaturesMinne,
        RefulgentArrow,
        Ignore
    }

    enum Song {
        None,
        MagesBallad,
        ArmysPaeon,
        TheWanderersMinuet,
    }

    enum Buff {
        StraighterShot = 0x7a,
        Barrage = 0x80,
        StraightShot = 0x82,
    }

    class SkillActivator {
        private readonly Random rand;

        public SkillActivator( System.Windows.Forms.Keys key, ida.FFXIV.Keyboard.KeyCode modifier, int hotbar, int index ) {
            this.Key = key;
            this.Modifier = modifier;

            this.Hotbar = hotbar;
            this.Index = index;

            this.rand = new Random();
        }

        public int Hotbar { get; }
        public int Index { get; }

        public System.Windows.Forms.Keys Key { get; }
        public ida.FFXIV.Keyboard.KeyCode Modifier { get; }

        public void Execute( ida.FFXIV.Client client ) {
            if( this.Key == System.Windows.Forms.Keys.None ) return;

            client.Keyboard.SetKey( ida.FFXIV.Keyboard.KeyCode.Alt, this.Modifier == ida.FFXIV.Keyboard.KeyCode.Alt );
            client.Keyboard.SetKey( ida.FFXIV.Keyboard.KeyCode.Shift, this.Modifier == ida.FFXIV.Keyboard.KeyCode.Shift );

            client.Keyboard.SendToggleKey( this.Key, 50 + this.rand.Next( -20, 21 ) );
        }

    }
}
