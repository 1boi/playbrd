﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace playbrd.Automation.BRD {
    class Dummy {

        private readonly ida.FFXIV.Client client;

        public Dummy( ida.FFXIV.Client client ) {
            this.client = client;
            this.GCD = 2450;
            this.sw = new System.Diagnostics.Stopwatch();
        }

        System.Diagnostics.Stopwatch sw;

        public int GCD {
            get => this.gcd;
            set {
                this.gcd = value;
                this.ogcd = value / 3;
            }
        }
        private int gcd;
        private int ogcd;

        public void Initialize() {
            //opener

            this.activeSong = Song.None;
            this.subGcdTick = 0;

            this.buffs = new int[( int )Enum.GetValues( typeof( Skill ) ).Cast<Skill>().Max() + 1];
            this.cooldowns = new int[( int )Enum.GetValues( typeof( Skill ) ).Cast<Skill>().Max() + 1];

            this.cooldowns[( int )Skill.Barrage] = 80000;
            this.cooldowns[( int )Skill.IronJaws] = 29000;
            this.cooldowns[( int )Skill.StraightShot] = 30000;
            this.cooldowns[( int )Skill.TheWanderersMinuet] = 80000;
            this.cooldowns[( int )Skill.MagesBallad] = 80000;
            this.cooldowns[( int )Skill.ArmysPaeon] = 80000;
            this.cooldowns[( int )Skill.BattleVoice] = 180000;

            this.gcds = new HashSet<Skill>();
            this.gcds.Add( Skill.CausticBite );
            this.gcds.Add( Skill.Stormbite );
            this.gcds.Add( Skill.StraightShot );
            this.gcds.Add( Skill.HeavyShot );
            this.gcds.Add( Skill.RefulgentArrow );
            this.gcds.Add( Skill.IronJaws );

            Skill[] opener = new Skill[] {
                Skill.CausticBite,
                Skill.RagingStrikes,
                Skill.Bloodletter,
                Skill.Stormbite,
                Skill.TheWanderersMinuet,
                Skill.EmpyrealArrow,
                Skill.StraightShot,
                Skill.None,
                Skill.Sidewinder,
                Skill.BattleVoice,
                Skill.IronJaws,
                Skill.None,
                Skill.None,
            };

            this.client.Update();
            var hotbarItems = opener.Where( x => x != Skill.None ).ToDictionary( x => x, y => this.GetItemFromHotbar( y ) );

            if( hotbarItems[Skill.BattleVoice].CooldownProgress != 0 ) {
                opener[9] = Skill.None;
            }
            
            var openerSongs = new Skill[] {
                Skill.TheWanderersMinuet,
                Skill.MagesBallad,
                Skill.ArmysPaeon
            };

            for( int i = 0; i < 3; i++ ) {
                var hotbarItem = this.GetItemFromHotbar( openerSongs[i] );
                if( hotbarItem.CooldownProgress != 0 ) {
                    this.buffs[( int )openerSongs[i]] = hotbarItem.CostOrRemainingCooldown * 1000;
                }
            }

            while(this.client.Entities.Target is null) {
                System.Threading.Thread.Sleep( 100 );
                    this.client.Update();
            }

            for( int i = 0; i < opener.Length; i++ ) {
                var sk = opener[i];

                if( sk == Skill.None ) {
                    if( this.client.Gauge.Charges >= 2 ) sk = Skill.PitchPerfect;
                }

                while( !this.Activate( sk ) ) {
                    System.Threading.Thread.Sleep( 50 );
                    this.client.Update();
                }

                if( sk != opener.Last() ) {
                    System.Threading.Thread.Sleep( 100 );
                }
            }

            this.target = this.client.Entities.Target.Pointer;

            this.activeSong = Song.TheWanderersMinuet;
            this.currentSkill = this.GetNextGcd();
            this.sw.Start();
        }

        private Song activeSong;

        private HashSet<Skill> gcds;

        int[] buffs;
        int[] cooldowns;

        int subGcdTick;
        private Skill currentSkill;
        private int aElapsed = 0;

        private long target;
        private int changeTargetCount = 0;
        private Skill[] changeTargetOpener = new Skill[] {
            Skill.CausticBite,
            Skill.Stormbite
        };

        public void Update() {
            this.sw.Stop();
            int elapsed = ( int )sw.ElapsedMilliseconds;
            this.sw.Restart();

            this.aElapsed += elapsed;

            for( int i = 0; i < this.buffs.Length; i++ ) {
                if( this.buffs[i] == 0 ) continue;
                this.buffs[i] -= elapsed;
                if( this.buffs[i] < 0 ) this.buffs[i] = 0;
            }

            if( this.Activate( this.currentSkill ) ) {
                if( this.subGcdTick == 0 ) {
                    int gcdProg = this.GetItemFromHotbar( Skill.HeavyShot ).CooldownProgress;
                    if( gcdProg > 90 || gcdProg == 0 ) {
                        this.currentSkill = GetNextGcd();
                        this.subGcdTick = 1;
                        this.aElapsed = 0;
                    } else {
                        this.currentSkill = Skill.None;
                    }
                } else {
                    this.currentSkill = GetNextOffGcd();
                    if( this.currentSkill == Skill.None ) {
                        if( this.aElapsed > this.ogcd * this.subGcdTick + this.ogcd * 3 / 4.0f ) {
                            this.subGcdTick += this.subGcdTick == 1 ? 1 : -2;
                        }
                    } else {
                        this.subGcdTick += this.subGcdTick == 1 ? 1 : -2;
                    }
                }
            }
        }

        public bool Activate( Skill sk ) {
            if( sk == Skill.Ignore ) return false;
            if( sk == Skill.None ) return true;

            var item = this.GetItemFromHotbar( sk );

            if( !item.IsAvailable ) return true;

            if( item.CooldownProgress != 0 ) {
                if( item.CooldownProgress >= 10 && !this.gcds.Contains( sk ) && item.CostOrRemainingCooldown * 1000 < this.GCD + this.GCD / 2 ) {
                    this.buffs[( int )sk] = item.CostOrRemainingCooldown * 1000;
                    return true;
                } else if( item.CooldownProgress < 10 ) {
                    if( this.cooldowns[( int )sk] != 0 ) this.buffs[( int )sk] = this.GetCooldown( sk );
                    return true;
                }
            }

            ActionMap.Map[sk].Execute( this.client );

            return false;
        }

        private Skill GetNextOffGcd() {

            if( this.subGcdTick == 2 ) {
                //activate barrage for refulgent
                if( this.GetItemFromHotbar( Skill.RefulgentArrow ).Highlighted ) {

                    //prevent barrage->IronJaws
                    if( this.buffs[( int )Skill.IronJaws] >= 2 * this.gcd + this.gcd / 2 ) {
                        var barrage = this.GetItemFromHotbar( Skill.Barrage );
                        if( barrage.CooldownProgress == 0 ) return Skill.Barrage;
                        if( barrage.CostOrRemainingCooldown * 1000 < this.GCD / 3 ) return Skill.Barrage;
                    }
                }
            }

            switch( this.activeSong ) {
                case Song.TheWanderersMinuet:
                    if( this.GetItemFromHotbar( Skill.RagingStrikes ).CooldownProgress == 0 ) {
                        return Skill.RagingStrikes;
                    }

                    if( this.subGcdTick == 1 ) {
                        if( this.GetItemFromHotbar( Skill.PitchPerfect ).Highlighted ) {
                            if( this.client.Gauge.Charges == 3 ) {
                                return Skill.PitchPerfect;
                            }
                            if( this.buffs[( int )Skill.TheWanderersMinuet] <= 50000 + this.gcd * 2 ) {
                                return Skill.PitchPerfect;
                            }
                        }
                    }

                    break;
                case Song.MagesBallad:
                    if( this.GetItemFromHotbar( Skill.Bloodletter ).CooldownProgress == 0 ) {
                        return Skill.Bloodletter;
                    }


                    break;
                case Song.ArmysPaeon:
                    break;
            }

            if( this.GetItemFromHotbar( Skill.Bloodletter ).CooldownProgress == 0 ) {
                return Skill.Bloodletter;
            }

            if( this.GetItemFromHotbar( Skill.EmpyrealArrow ).CooldownProgress == 0 ) {
                if( this.activeSong != Song.MagesBallad ) {
                    if( this.activeSong == Song.TheWanderersMinuet ) {
                        if( this.buffs[( int )Skill.TheWanderersMinuet] > 50500 + this.GCD * 2 ) {
                            if( this.client.Gauge.Charges < 3 ) return Skill.EmpyrealArrow;
                        } else {
                            if( this.client.Gauge.Charges != 0 ) return Skill.EmpyrealArrow;
                        }
                    } else {
                        return Skill.EmpyrealArrow;
                    }
                } else if( this.GetItemFromHotbar( Skill.Bloodletter ).CooldownProgress < 85 ) {
                    return Skill.EmpyrealArrow;
                }
            }

            if( this.GetItemFromHotbar( Skill.Sidewinder ).CooldownProgress == 0 ) {
                return Skill.Sidewinder;
            }

            if( this.GetItemFromHotbar( Skill.MiserysEnd ).CooldownProgress == 0 && this.GetItemFromHotbar( Skill.MiserysEnd ).Highlighted ) {
                return Skill.MiserysEnd;
            }

            switch( this.activeSong ) {
                case Song.TheWanderersMinuet:
                    if( this.buffs[( int )Skill.TheWanderersMinuet] < 50500 ) {
                        this.activeSong = Song.MagesBallad;
                        return Skill.MagesBallad;
                    }
                    break;
                case Song.MagesBallad:
                    if( this.buffs[( int )Skill.MagesBallad] < 50500 ) {
                        this.activeSong = Song.ArmysPaeon;
                        return Skill.ArmysPaeon;
                    }
                    break;
                case Song.ArmysPaeon:
                    if( this.buffs[( int )Skill.ArmysPaeon] < 60500 ) {
                        this.activeSong = Song.TheWanderersMinuet;
                        return Skill.TheWanderersMinuet;
                    }
                    break;
            }

            //   if( this.GetItemFromHotbar( Skill.BattleVoice ).CooldownProgress == 0 ) {
            //    return Skill.BattleVoice;
            //   }

            return Skill.None;
        }

        private Skill GetNextGcd() {
            if( this.client.Entities.Target is null ) {
                return Skill.Ignore;
            }

            var refulgent = this.GetItemFromHotbar( Skill.RefulgentArrow );
            var barrage = this.GetItemFromHotbar( Skill.Barrage );

            if( this.buffs[( int )Skill.StraightShot] < this.gcd * 2 + this.gcd / 2 ) {

                if( this.buffs[( int )Skill.StraightShot] < this.gcd + this.gcd / 2 ) {
                    this.buffs[( int )Skill.StraightShot] = this.GetCooldown( Skill.StraightShot );
                    return Skill.StraightShot;
                }

                if( refulgent.Highlighted ) {
                    //barrage available
                    if( barrage.CooldownProgress == 0 ) return Skill.StraightShot;
                    //barrage ready in next 2 gcds
                    if( barrage.CostOrRemainingCooldown * 1000 < this.GCD + this.gcd / 2 ) return Skill.StraightShot;
                    return Skill.RefulgentArrow;
                }

                return Skill.StraightShot;
            }
            if( this.target != this.client.Entities.Target.Pointer ) {
                switch( this.changeTargetCount ) {
                    case 0:
                        this.changeTargetCount++;
                        return Skill.Stormbite;
                    case 1:
                        this.changeTargetCount++;
                        return Skill.CausticBite;
                    case 2:
                        this.changeTargetCount = 0;
                        this.target = this.client.Entities.Target.Pointer;
                        break;
                }
            }

            if( this.buffs[( int )Skill.IronJaws] < 2 * this.gcd + this.gcd / 2 ) {
                if( this.buffs[( int )Skill.IronJaws] < this.gcd + this.gcd / 2 ) {
                    this.buffs[( int )Skill.IronJaws] = this.GetCooldown( Skill.IronJaws );
                    return Skill.IronJaws;
                }

                if( !this.client.Entities.Self.Effects.Any( x => x.ID == ( int )Buff.Barrage ) ) {
                    return Skill.IronJaws;
                }
            }

            if( refulgent.Highlighted ) {
                //refulgent available
                if( this.client.Entities.Self.Effects.FirstOrDefault( x => x.ID == ( int )Buff.Barrage ).ID == ( int )Buff.Barrage ) {
                    //barrage up
                    return Skill.RefulgentArrow;
                }
                //barrage not up, but available within weave for next gcd
                if( barrage.CostOrRemainingCooldown * 1000 < this.GCD * 2 ) {
                    //delay refulgent for barrage
                    return Skill.HeavyShot;
                }

                //barrage not up, not available
                return Skill.RefulgentArrow;
            }

            return Skill.HeavyShot;
        }

        private int GetCooldown( Skill sk ) {
            return this.cooldowns[( int )sk];
        }


        private ida.FFXIV.Hud.HotbarItem GetItemFromHotbar( Skill sk ) {
            return this.client.Hud.Hotbars[ActionMap.Map[sk].Hotbar].Slots[ActionMap.Map[sk].Index];
        }
    }


}
